import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        ExtravtArgumentScreen.routeName: (context) =>
            const ExtravtArgumentScreen(),
      },
    );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Screen'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, ExtravtArgumentScreen.routeName,
                arguments: ScreenArguments('Extract Argument',
                    'This message is extracted in the build method'));
          },
          child: Text('Navigate to screen that extracts argument'),
        ),
      ),
    );
  }
}

class ScreenArguments {
  final String title;
  final String message;

  ScreenArguments(this.title, this.message);
}

class ExtravtArgumentScreen extends StatelessWidget {
  const ExtravtArgumentScreen({Key? key}) : super(key: key);
  static const routeName = '/extractArgument';

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ScreenArguments;
    return Scaffold(
      appBar: AppBar(title: Text('')),
      body: Center(
        child: Text(''),
      ),
    );
  }
}
